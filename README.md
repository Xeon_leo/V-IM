
### 需要购买的阿里云的同学 请点击支持 [阿里云优惠券2000元](https://www.aliyun.com/activity/new/index?userCode=d4l0ykh3)
### 声明：切勿使用本软件从事任何违法事宜，使用本软件产生的任何后果皆由使用者承担，本软件及开发者概不承担任何责任。
### 加微:![](doc/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240301144740.jpg) 务必备注：v-im 并且附上点赞的 gitee 用户名，不点赞不加！
### 企业版源码微信联系（有偿）。
### web版本测试地址：http://101.200.151.183   需要在网页版上面注册用户。
### 企业版下载(windows)：https://v-im-oss.oss-cn-beijing.aliyuncs.com/auto-updates/V-IM-2.5.1-setup.exe
### 企业版下载(linux)：https://v-im-oss.oss-cn-beijing.aliyuncs.com/auto-updates/V-IM_2.5.3_amd64.deb
### 企业版安卓APP![](https://gitee.com/lele-666/V-IM/raw/master/doc/ewm.png)

### Create by  [webstorm and intellij IDEA]
### 说明
>   系统是在RuoYi-vue(https://gitee.com/y_project/RuoYi-Vue) 的基础上开发的，但是把数据库操作改成mybatis-plus,原先的是mybatis（如果你想完全迁移到RuoYi系统里面，可能还需要一定的工作量）。

### 结构
>   1. v-im-pc 是聊天客户端，支持打包成exe 和 h5网页。
>   2. v-im-server 是服务端代码，集成了ruoyi的模块。
>   3. RuoYi-ui-vue3 是ruoyi管理系统的前端代码。
>   4. doc 下面有数据库等。
>   5. 1、2、3都是要启动的，务必先启动2
>   6. 开源版客户端连接 101.200.151.183 会报错，版本不一样，需要自己部署服务端，然后修改客户端的配置文件。
#### 使用部署文档
https://juejin.cn/user/3843548381983191/posts

### 截图

![消息列表/聊天](https://gitee.com/lele-666/V-IM/raw/master/doc/img/1.png)
![好友](https://gitee.com/lele-666/V-IM/raw/master/doc/img/2.png)
![组织](https://gitee.com/lele-666/V-IM/raw/master/doc/img/3.png)
![群组](https://gitee.com/lele-666/V-IM/raw/master/doc/img/4.png)
![添加好友](https://gitee.com/lele-666/V-IM/raw/master/doc/img/5.png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(1).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(2).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(3).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(4).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(5).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(6).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(7).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(8).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(9).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(10).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(11).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/(12).png)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/s1.jpg)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/s2.jpg)
![手机](https://gitee.com/alyouge/V-IM/raw/master/doc/uniapp/s3.jpg)
 
 

### 功能点
![输入图片说明](doc/20240403110248.png)

### 参考项目及技术
> 1. RuoYi-vue（https://gitee.com/y_project/RuoYi-Vue）
> 2. layIM（主要是聊天表情，文件处理方面）。
> 3. 使用SpringBoot、oauth2.0、t-io 开发后端服务。
> 4. vue3.0、element-plus、typescript开发前端。
> 5. 界面高仿微信，如有侵权请告知。
> 6. 其他：使用 fetch 发送ajax 请求，支持跨域，electron 支持打包成为exe，也支持linux 和 mac 目前还没测试，有条件的同学可以测试。

### 交流授权
>  1. 如果您觉得好用，可以给点个star，或者给个捐赠。
>  2. 如需定制或者私有化部署，请加微:zkp_java。
>  3. 商用请捐赠并在捐赠【留言】里留下公司名称，没有留公司名商用视为侵权。